---
layout: "default"
---
# Privacy Policy

Privacy Policy of the Straightway project - Status: October 12th 2019

1. The Straightway project, represented by Kai Bösefeldt (more details: see imprint), informs by this privacy policy according to art. 13 of the European General Data Protection Regulation (GDPR) that it collects and processes personal data about the following persons:

    * Users of the website https://pages.codeberg.org/straightway.
    * Contributiors for the Straightway project

2. Personal data required for the provision of the website and the administration of the Straightway project is collected and/or stored via [codeberg.org](https://codeberg.org) as service provider. If data for the Straightway project is processed by [codeberg.org](https://codeberg.org), this ensures that the data remains adequately protected and is not misused. See the [Codeberg privacy statement](https://codeberg.org/codeberg/org/src/branch/master/PrivacyPolicy.md) for more information.

3. Each time a user accesses the Straightway website and each time a file is retrieved, technical information may be collected from visitors to the website, including IP addresses, in order to ensure the security and integrity of the website and the service. See the [Codeberg privacy statement](https://codeberg.org/codeberg/org/src/branch/master/PrivacyPolicy.md) for more information.

4. The legal basis for the processing of the signatories' data is their explicit consent pursuant to art. 6 para. 1 lit. a GDPR. The data of the persons who use the website of the Straightway will be processed on the basis of Art. 6 para. 1 lit. f) GDPR. The legitimate interest in the processing pursuant to Art. 6 para. 1 lit. f) consists in the provision of the website and the organisation of development process of the Straightway software.

    Where data processing is carried out with the consent of the data subjects, Art. 6 para. 1 lit. a) GDPR is relevant. Such consent may be revoked at any time with effect for the future.

9. This privacy policy shall be effective from 12 October 2019.

The data protection regulations of the [European Academy for Freedom of Information and Data Protection (EAID)](https://www.eaid-berlin.de/datenschutzerklaerung-2/) served as a basis for these data protection regulations.

[Back](index.html)
